<?php

/**
 *  Autoload used validators classes.
 */
spl_autoload_register(function($class_name)
{
    require_once('validators/'.$class_name.'.php');
});

/**
 * Validators composition class.
 * 
 * @author Artur Olshevsky
 */
class ValidatorsComposition
{
    private $validators = array();
    private $errors = array();
    private $valid = true;
    
    /**
     * Add new validator object to the list.
     * 
     * @param type $validator - validator object
     */
    public function add($validator = null)
    {
        if(is_object($validator) && method_exists($validator, 'execute'))
        {
            array_push($this->validators, $validator);
        }
    }
    
    /**
     * Run all added validators one by one.
     * Return true if all validators return true.
     * 
     * @return boolean
     */
    public function execute()
    {
        foreach ($this->validators as $validator)
        {
            if(!$validator->execute())
            {
                $this->errors[$validator->getName()] = $validator->getError();
                $this->valid = false;
            }
        }
        
        return $this->valid;
    }
    
    /**
     * Return validator status.
     * 
     * @return type bool
     */
    public function isValid()
    {
        return $this->valid;
    }
    
    /**
     * Return validators errors.
     * 
     * @return type array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    
    /**
     * Return first validator error.
     * 
     * @return type array
     */
    public function getFirstError()
    {
        $errors = array_values($this->errors);
        return $errors[0];
    }
}
?>
