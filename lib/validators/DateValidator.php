<?php

/**
 * Date Validator.
 *
 * Available options:
 * (boolean) required - is field required
 * 
 * Available error codes:
 * required     - required field
 * incorrect    - incorrect date format
 * exist        - if date real exist
 * 
 * @author Artur Olshevsky
 */
class DateValidator extends BaseValidator
{
    const PATTERN1_REGEX = '/^\d{2}\/\d{2}\/\d{4}$/'; // DD/MM/YYYY
    
    public function __construct($value, $name, $options = array(), $messages = array())
    {
        $messages = array_merge(array('incorrect' => 'Date format is incorrect.', 'exist' => 'Given date does not exist.'), $messages);
        parent::__construct($value, $name, $options, $messages);
    }
    
    public function execute()
    {
        if($this->value == '')
        {
            if($this->options['required'] == true)
            {
                $this->error = $this->messages['required'];
                return $this->valid = false;
            }
        }
        else
        {
            if(!preg_match(self::PATTERN1_REGEX, $this->value))
            {
                $this->error = $this->messages['incorrect'];
                return $this->valid = false;
            }
            else
            {
                $date = explode('/', $this->value);
                
                if(!checkdate($date[1], $date[0], $date[2]))
                {
                    $this->error = $this->messages['exist'];
                    return $this->valid = false;
                }
            }
        }

        return $this->valid;
    }
}

?>
