<?php
/**
 * Abstract Base Validator class. Normally You should extend this class.
 *
 * Base available options:
 * (boolean) required - is field required
 * 
 * Available error codes:
 * required 
 * 
 * @author Artur Olshevsky
 */
abstract class BaseValidator
{
    protected $value = null;
    protected $name = null;
    protected $options = array();
    protected $messages = array();
    protected $valid = true;
    protected $error = null;
    
    /**
     * Base validator constructer.
     * 
     * @param type $value - value to validate
     * @param type $name - name of field which is validated
     * @param type $options - array of validation options
     * @param type $messages - array of error messages
     */
    public function __construct($value, $name, $options = array(), $messages = array())
    {
        $this->value = $value;
        $this->name = $name;
        $this->options = array_merge(array('required' => false), $options);
        $this->messages = array_merge(array('required' => 'This field is required.'), $messages);
    }
    
    /**
     * Main method which validates value. 
     * Here you place specific rules of your validator.
     * Put result in $this->valid and return it.
     * 
     * @return type bool
     */
    public abstract function execute();
    
    /**
     * Return validator status.
     *  
     * @return type bool
     */
    public function isValid()
    {
        return $this->valid;
    }
    
    /**
     * Return validation error.
     *  
     * @return type string
     */
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * Return field name.
     *  
     * @return type string
     */
    public function getName()
    {
        return $this->name;
    }
}

?>
