<?php

/**
 * Validates an integer.
 * 
 * Available options:
 * (boolean) required - is field required
 * 
 * Available error codes:
 * required 
 * incorrect
 * 
 * @author Artur Olshevsky
 */
class IntegerValidator extends BaseValidator
{   
    public function __construct($value, $name, $options = array(), $messages = array())
    {
        $messages = array_merge(array('incorrect' => 'Number format is incorrect.'), $messages);
        parent::__construct($value, $name, $options, $messages);
    }
    
    public function execute()
    {
        if($this->value == '')
        {
            if($this->options['required'] == true)
            {
                $this->error = $this->messages['required'];
                return $this->valid = false;
            }
        }
        else
        {
            if(!is_int($this->value))
            {
                $this->error = $this->messages['incorrect'];
                return $this->valid = false;
            }
        }

        return $this->valid;
    }
}

?>
