<?php

/**
 * String Validator.
 *
 * Available options:
 * (boolean) required - is field required
 * (array) allowed_values 
 * 
 * Available error codes:
 * required 
 * allowed_values
 * 
 * @author Artur Olshevsky
 */
class StringValidator extends BaseValidator
{
    public function __construct($value, $name, $options = array(), $messages = array())
    {
        $options = array_merge(array('allowed_values' => null), $options);
        $messages = array_merge(array('incorrect' => 'Number format is incorrect.', 'allowed_values' => 'This value is not allowed.'), $messages);
        parent::__construct($value, $name, $options, $messages);
    }
    
    
    public function execute()
    {   
        if($this->value == '')
        {
            if($this->options['required'] == true)
            {
                $this->error = $this->messages['required'];
                return $this->valid = false;
            }
        }
        else
        {
            if($this->options['allowed_values'] != null && gettype($this->options['allowed_values']) == 'array')
            {
                if(!in_array($this->value, $this->options['allowed_values']))
                {
                    $this->error = $this->messages['allowed_values'];
                    $this->valid = false;
                }
            }
        }
        
        
        return $this->valid;
    }
}

?>
