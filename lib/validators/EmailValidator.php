<?php

/**
 * Email Validator.
 *
 * Available options:
 * (boolean) required - is field required
 * 
 * Available error codes:
 * required 
 * incorrect
 * 
 * @author Artur Olshevsky
 */
class EmailValidator extends BaseValidator
{
    const EMAIL_REGEX = '/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i';
    
    public function __construct($value, $name, $options = array(), $messages = array())
    {
        $messages = array_merge(array('incorrect' => 'Email format is incorrect.'), $messages);
        parent::__construct($value, $name, $options, $messages);
    }

    public function execute()
    {
        if($this->value == '')
        {
            if($this->options['required'] == true)
            {
                $this->error = $this->messages['required'];
                return $this->valid = false;
            }
        }
        else
        {
            if(!preg_match(self::EMAIL_REGEX, $this->value))
            {
                $this->error = $this->messages['incorrect'];
                return $this->valid = false;
            }
        }

        return $this->valid;
    }
}

?>
