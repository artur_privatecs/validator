<?php

/**
 * Validates a number (integer or float). Support scientific notation.
 * 
 * Available options:
 * (boolean) required - is field required
 * 
 * Available error codes:
 * required 
 * incorrect
 * 
 * @author Artur Olshevsky
 */
class DecimalValidator extends BaseValidator
{
    const DECIMAL_REGEX = '/^[0-9]+(\.[0-9]{1,9})?$/'; // to exclude scientific notation numbers
    
    public function __construct($value, $name, $options = array(), $messages = array())
    {
        $messages = array_merge(array('incorrect' => 'Number format is incorrect.'), $messages);
        parent::__construct($value, $name, $options, $messages);
    }
    
    public function execute()
    {
        if($this->value == '')
        {
            if($this->options['required'] == true)
            {
                $this->error = $this->messages['required'];
                return $this->valid = false;
            }
        }
        else
        {
            if(!preg_match(self::DECIMAL_REGEX, $this->value))
            {
                $this->error = $this->messages['incorrect'];
                return $this->valid = false;
            }
        }

        return $this->valid;
    }
}

?>
